#!/bin/bash
#SBATCH -p XXXqueueXXX
#SBATCH -J XXXnameXXX
#SBATCH --ntasks=XXXdedicatedXXX
#SBATCH -o XXXnameXXX/run.out
#SBATCH -e XXXnameXXX/run.err
#SBATCH --gres=gpu:XXXextra1XXX
#SBATCH --mail-type=END,FAIL
#SBATCH -N 1
#SBATCH --mem=XXXextra2XXX
#SBATCH --time=XXXextra3XXX
#SBATCH XXXextra5XXX
##SBATCH -x nodes_to_exclude
##SBATCH -w nodes_to_reserve



export MODULEPATH=/opt/ohpc/pub/modulefiles:/g/easybuild/x86_64/CentOS/7/haswell/modules/all
source /opt/ohpc/admin/lmod/lmod/init/bash

module purge; module load RELION
module list RELION CUDA ctffind Gctf matplotlib MotionCor2 ResMap summovie unblur 2>&1
mpirun -n XXXmpinodesXXX XXXcommandXXX

#slurm script template version: 3.0-beta-EMBLv.000333, $EBROOTRELION/etc/templates/gpu.slurm.script
#module name: RELION/3.0-beta-EMBLv.0014_20190110_01_943a876_a-foss-2018b-cu92
