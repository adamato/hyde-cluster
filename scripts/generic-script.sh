#!/bin/bash
#SBATCH -N 1
#SBATCH -p gpu
#SBATCH --ntasks=16
# #SBATCH --gres=gpu:1
date
hostname
env
export MODULEPATH=/opt/ohpc/pub/modulefiles:/g/easybuild/x86_64/CentOS/7/haswell/modules/all
. /opt/ohpc/admin/lmod/lmod/init/bash
module avail
echo sleeping for 5 minutes
sleep 300
echo done
